 $(document).ready(function(){
     var MenuShowTimer;
     $('.buy').hover(
         function(e) {
             clearTimeout(MenuShowTimer);
             $(this).addClass('animated bounce');
         },
         function(e) {
             MenuShowTimer=setTimeout(
                 (function (Obj){
                     return function () {
                         $(Obj).removeClass('animated bounce');
                     };
                 })(this)
                 ,500)}
     );

});

$(document).ready(function() {
 $('.zoom').addClass("visible").viewportChecker({
     classToAdd: 'hidden  animated zoomIn',
     offset: 100
 });
});